FROM node:argon
RUN apt-get update -qq
RUN apt-get install -yq less nano jq net-tools
RUN npm install -g -q json
RUN export CB=containerbuddy-1.1.0 &&\
    mkdir -p /opt/containerbuddy && \
    curl -Lo /tmp/${CB}.tar.gz \
    https://github.com/joyent/containerbuddy/releases/download/1.1.0/containerbuddy-1.1.0.tar.gz && \
    tar xzf /tmp/${CB}.tar.gz -C /tmp && \
    rm /tmp/${CB}.tar.gz && \
    mv /tmp/containerbuddy /opt/containerbuddy/
ONBUILD ADD /opt/containerbuddy /opt/containerbuddy
ONBUILD COPY dist  /application/
ONBUILD WORKDIR /application
ONBUILD ADD .npmrc .
ONBUILD RUN  npm install --production -q
ONBUILD RUN  rm .npmrc
